# Purpose

Illustrates many hashing techniques to translate a string into a number.

# Build Configuration

We use the `gnatkr` tool to get a (mostly) unique executable name on 16 (max) caracter length.
See the [gnatkr documentation](https://docs.adacore.com/gnat_ugn-docs/html/gnat_ugn/gnat_ugn/the_gnat_compilation_model.html#file-name-krunching-with-gnatkr) for the File Name Krunching algorithm.

```shell
$ gnatkr gaudry_algorithms_crypto_hashings.adb 16
```

```ada
   for Main use ("gaudry_algorithms_crypto_hashings.adb");

   for Executable ("gaudry_algorithms_crypto_hashings.adb") use "gaudalgocryphash";  -- gnatkr *.adb 16
```

# Build

```shell
$ gprbuild
```

or

```shell
$ alr build
```

Results in:

```
build
└── development (or validation)(or release)
    └── x86_64-apple-darwin22.1.0
        ├── bin
        └── obj
```

# Run

### On Unix-like

There could be a soft-link to the `build/*/*/bin` directory

```shell
$ ln -sv build/development/x86_64-apple-darwin22.1.0/bin bin
```

then,

```shell
$ bin/gaudalgocryphash
```

### On Window$

?? up to you ...

```
build\development\x86_64\bin\gaudalgocryphash.exe
```
