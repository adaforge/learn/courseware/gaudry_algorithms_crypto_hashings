--
--
--
--  illustrates many hashing techniques to translate a string into a number
--
--



with Interfaces;use Interfaces;
with Ada.Text_Io;
with Unchecked_Conversion;

procedure Gaudry_Algorithms_Crypto_Hashings
   is

   subtype U32 is Interfaces.Unsigned_32;
   subtype U64 is Interfaces.Unsigned_64;
   package Atio renames Ada.Text_Io;

   --|
   --|         |_____________________|
   --|         |                     |
   --|         |    PRINT_INTEGER    |
   --|         |_____________________|
   --|         |                     |
   --|
   function Print_Integer(X     : in U64;
                          Base  : in Positive := 10;
                          Width : in Positive)
      return String
      is
      package Translate is new Atio.Modular_Io(U64);
      Size              : constant Natural   := 70;
      Result            : String(1 .. Size)  := (others => ' ');
      Output            : String(1 .. Width) := (others=> '0');
      F                 : Natural            := 0;
      Spread            : Natural            := 0;
      Ascii_0           : constant Character := Character'Val(16#30#);
      Ascii_9           : constant Character := Character'Val(16#39#);
      Ascii_Uppercase_A : constant Character := Character'Val(16#41#);
      Ascii_Uppercase_F : constant Character := Character'Val(16#41#);
      Ascii_Lowercase_A : constant Character := Character'Val(16#61#);
      Ascii_Lowercase_F : constant Character := Character'Val(16#66#);

      Print_Integer_Width_Too_Short : exception;

      -- NO STRANGE CHARACTER AS INPUT
      subtype Digit is Character with Static_Predicate => Digit in
      Ascii_0 .. Ascii_9 | Ascii_Uppercase_A .. Ascii_Uppercase_F | Ascii_Lowercase_A .. Ascii_Lowercase_F;

      begin

      Translate.Put(To   => Result,
                    Item => X     ,
                    Base => Base);

      Loop_On_All_Characters :
      for H in Result'Range loop
         exit Loop_On_All_Characters when Result(H) in Digit;
         F := H;
      end loop Loop_On_All_Characters;

      case Base
         is
         when 2 => Spread := Size - F - 4;

         if Spread > Width
            then
            raise Print_Integer_Width_Too_Short;
         end if;

         Output(Width - Spread .. Width) := Result(F + 3 .. Size - 1);
         return Output;

         when 10 => Spread := Size - F - 1;

         if Spread > Width
            then
            raise Print_Integer_Width_Too_Short;
         end if;

         Output(Width - Spread .. Width) := Result(F + 1 .. Size);
         return Output;

         when 16 => Spread := Size - F - 5;

         if Spread > Width
            then
            raise Print_Integer_Width_Too_Short;
         end if;

         Output(Width - Spread .. Width) := Result(F + 4 .. Size - 1);
         return Output;

         when others=> return "BASE" & Base'Img & " NOT (YET)  CARED FOR";

      end case;

   end Print_Integer;

   ---=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-
   ---=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-
   ---=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-
   ---=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-
   ---=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-
   ---=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-
   ---=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-
   --
   --               LIST OF MANY HASH FUNCTIONS FOR STRINGS  FROM VARIOUS ORIGINS AND THEIR TEST
   --
   ---=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-
   ---=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-
   ---=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-
   ---=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-
   ---=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-
   ---=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-
   ---=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-
   ---=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-
   ---=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-
   --
   A_Prime : constant U64 := 2305843009213694443;


   ---------------------
   --  LEFT_ROTATION  --
   ---------------------

   function Left_Rotation(A : in U64;
                          D : in U64)
      return U64
      is
      -- USING THE BUILT IN FUNCTION OF THE ADA COMPILER IN INTERFACES
      Dummy    : Interfaces.Unsigned_64 := Interfaces.Unsigned_64(A);
      Exponent : constant Natural       := Natural(D);
      Result   : Interfaces.Unsigned_64;
      begin
      Result := Interfaces.Rotate_Left(Value => Dummy, Amount => Exponent);
      return U64(Result);
   end Left_Rotation;

   ----------------------
   --  RIGHT_ROTATION  --
   ----------------------

   function Right_Rotation(A : in U64;
                           D : in U64)
      return U64
      is
      -- USING THE BUILT IN FUNCTION OF THE ADA COMPILER IN INTERFACES
      Dummy    : Interfaces.Unsigned_64 := Interfaces.Unsigned_64(A);
      Exponent : constant Natural       := Natural(D);
      Result   : Interfaces.Unsigned_64;
      begin
      Result := Interfaces.Rotate_Right(Value => Dummy, Amount => Exponent);
      return U64(Result);
   end Right_Rotation;

   --------------
   --  RSHASH  --
   --------------

   function Rshash(S : in String)
      return U64
      is
      Char_Value : U64          := 0;
      Hash       : U64          := A_Prime;
      B          : constant U64 := 378551;
      A          : U64          := 63689;
      begin
      Loop_On_Every_Character :
      for K in S'Range loop
         Char_Value := Character'Pos(S(K));
         Hash       := Hash * A + Char_Value;
         A          := A    * B;
      end loop Loop_On_Every_Character;
      return Hash;
   end Rshash;

   --------------
   --  JSHASH  --
   --------------

   function Jshash(S : in String)
      return U64
      is
      Hash       : U64 := A_Prime;-- 1315423911;
      Char_Value : U64;
      begin
      Loop_On_Every_Character :
      for K in S'Range loop
         Char_Value := Character'Pos(S(K));
         Hash       := Hash xor (Hash * 2 ** 5 + Char_Value + Hash / 2 ** 2);
      end loop Loop_On_Every_Character;
      return Hash;
   end Jshash;

   ---------------
   --  PJWHASH  --
   ---------------

   function Pjwhash(S : in String)
      return U64
      is
      H    : U64              := A_Prime;
      Bits : constant Natural := U64'Size;
      High : U64              := 0;
      begin
      Loop_On_Every_Character :
      for I in S'Range loop
         H    := H * 2 ** (Bits / 8) + Character'Pos(S(I));
         High := H and 16#F000000000000000#;
         if High /= 0 then
            H := H xor (High / 2 ** (Bits * 3 / 4));
            H := H and (not High);
         end if;
      end loop Loop_On_Every_Character;
      return H;
   end Pjwhash;

   ---------------
   --  ELFHASH  --
   ---------------

   function Elfhash(S : in String)
      return U64
      is
      Hash       : U64 := A_Prime;
      X          : U64 := 0;
      Char_Value : U64;
      begin
      Loop_On_Every_Character :
      for K in S'Range loop
         Char_Value := Character'Pos(S(K));
         Hash       := Hash * 2 ** 4 + Char_Value;
         X          := Hash and 16#F000000000000000#;
         if X /= 0
            then
            Hash := Hash xor X / 2 ** 24;
         end if;
         Hash := Hash and not X;
      end loop Loop_On_Every_Character;
      return Hash;
   end Elfhash;

   ----------------
   --  BKDRHASH  --
   ----------------

   function Bkdrhash(S : in String)
      return U64
      is
      Hash       : U64          := A_Prime;
      Seed       : constant U64 := 16#2B33FFE62185A82B#;
      Char_Value : U64;
      begin
      Loop_On_Every_Character :
      for K in S'Range loop
         Char_Value := Character'Pos(S(K));
         Hash       := Hash * Seed + Char_Value;
      end loop Loop_On_Every_Character;
      return Hash;
   end Bkdrhash;

   ---------------
   --  DEKHASH  --
   ---------------

   function Dekhash(S : in String)
      return U64
      is
      Hash       : U64 := S'Length;
      Char_Value : U64;
      begin
      Loop_On_Every_Character :
      for K in S'Range loop
         Char_Value := Character'Pos(S(K));
         Hash       := Hash * 2 ** 5 xor Hash / 2 ** 27 xor Char_Value;
      end loop Loop_On_Every_Character;
      return Hash;
   end Dekhash;

   --------------
   --  BPHASH  --
   --------------

   function Bphash(S : in String)
      return U64
      is
      Hash       : U64 := A_Prime;
      Char_Value : U64;
      begin
      Loop_On_Every_Character :
      for K in S'Range loop
         Char_Value := Character'Pos(S(K));
         Hash       := Hash * 2 ** 7 xor Char_Value;
      end loop Loop_On_Every_Character;
      return Hash;
   end Bphash;

   --------------
   --  APHASH  --
   --------------

   function Aphash(S : in String)
      return U64
      is
      Hash       : U64 := 16#AAAAAAAAAAAAAAAA#;
      Char_Value : U64;
      Dummy      : U64;
      begin
      Loop_On_Every_Character :
      for K in S'Range loop
         Char_Value := Character'Pos(S(K));
         Dummy      := U64(K);
         if (Dummy and 1) = 0
            then
            Hash := Hash xor Hash * 2 ** 7 xor Char_Value * Hash / 2 ** 3;
         else
            Hash := Hash xor Hash * 2 ** 11 + Char_Value xor Hash / 2 ** 5;
         end if;
      end loop Loop_On_Every_Character;
      return Hash;
   end Aphash;

   ------------
   --  DJB2  --
   ------------

   function Djb2(S : in String)
      return U64
      is
      Hash       : U64 := A_Prime;
      Char_Value : U64;
      begin
      Loop_On_Every_Character :
      for K in S'Range loop
         Char_Value := Character'Pos(S(K));
         Hash       := Hash * 33 xor Char_Value;
      end loop Loop_On_Every_Character;
      return Hash;
   end Djb2;

   ------------
   --  SDBM  --
   ------------

   function Sdbm(S : in String)
      return U64
      is
      Hash       : U64 := A_Prime;
      Char_Value : U64;
      begin
      Loop_On_Every_Character :
      for K in S'Range loop
         Char_Value := Character'Pos(S(K));
         Hash       := Char_Value + Hash * 65599;
      end loop Loop_On_Every_Character;
      return Hash;
   end Sdbm;

   -------------
   --  FNV_1  --
   -------------

   function Fnv_1(S : in String)
      return U64
      is
      Fnv_Offset_Basis : constant U64 := 16#CBF29CE484222325#;
      Fnv_Prime        : constant U64 := 16#00000100000001B3#;
      Hash             : U64          := A_Prime;
      Char_Value       : U64          := 0;
      begin
      Hash                    := Fnv_Offset_Basis;
      Loop_On_Every_Character :
      for K in S'Range loop
         Char_Value := Character'Pos(S(K));
         Hash       := Hash * Fnv_Prime;
         Hash       := Char_Value xor Hash;
      end loop Loop_On_Every_Character;
      return Hash;
   end Fnv_1;

   ---------------------
   --  SUPERFASTHASH  --
   ---------------------

   function Superfasthash(Adata : in String)
      return U64
      is
      Adatalength    : U64      := Adata'Length;
      Temppart       : U64      := 0;
      Remainingbytes : U64      := 0;
      Result         : U64      := A_Prime;
      Temp_1         : U64      := 0;
      Temp_2         : U64      := 0;
      K              : Positive := 1;
      begin

      if "" = Adata or else 0 = Adatalength
         then
         return 0;
      end if;

      Result         := Adatalength;
      Remainingbytes := Adatalength and 3;
      Adatalength    := Adatalength / 2 ** 2;

      Loop_On_Every_Character :
      while Adatalength > 0 loop

         Temp_1      := Character'Pos(Adata(K));
         Result      := Result                          xor Temp_1;
         Temppart    := Temp_1 + 2                      xor Temp_1 * 2 ** 11 xor Result;
         Result      := Result                * 2 ** 16 xor Temppart;
         Result      := Result                * 2 ** 11;
         Adatalength := Adatalength - 1;
         K           := 1 + K;
      end loop Loop_On_Every_Character;

      case Remainingbytes
         is
         when 3 =>
         Temp_2 := Character'Pos(Adata(Adata'First));
         Temp_1 := Character'Pos(Adata(Adata'First + 1));
         Result := Result                          + Temp_1 or Temp_1 * 2 ** 8;
         Result := Result xor Result         * 2 ** 16;
         Result := Result xor Temp_2         * 2 ** 18;
         Result := Result + Result / 2 ** 11;
         when 2 =>
         Temp_1 := Character'Pos(Adata(Adata'First + 1));
         Temp_2 := Character'Pos(Adata(Adata'First));
         Result := Result + Temp_1 or Temp_2 * 2 ** 8;
         Result := Result xor Result         * 2 ** 11;
         Result := Result + Result / 2 ** 17;
         when 1 =>
         Result := Result + Character'Pos(Adata(Adata'First));
         Result := Result + Result * 2 ** 10;
         Result := Result + Result / 2;
         when others=> null;
      end case;

      Result := Result xor Result * 2 ** 3;
      Result := Result + Result / 2 ** 5;
      Result := Result xor Result * 2 ** 4;
      Result := Result + Result / 2 ** 17;
      Result := Result xor Result * 2 ** 25;
      Result := Result + Result / 2 ** 6;

      return Result;

   end Superfasthash;

   ----------------
   --  XORSHIFT  --
   ----------------

   function Xorshift(S : in String)
      return U64
      is
      --|              |=-=-=-=-=-=-=-=|
      --|              |       RL      |
      --|              |=-=-=-=-=-=-=-=|
      function Rl(X : in U64;
                  B : in Natural)
         return U64
         is
         begin
         return (X / 2 ** B) or (X * 2 ** (X'Size - B));
      end Rl;
      --
      --                MAIN
      --
      Z : U64 := 2 ** 31 - 1;
      begin
      for W in S'Range loop
         Z := Z + Character'Pos(S(W));
      end loop;
      Z := Z xor (Z * 2 ** 21);
      Z := Z xor (Rl(Z, 35));
      Z := Z xor (Z * 2 ** 4);
      return Z;
   end Xorshift;

   --  LONG_RANDOM  --
   -------------------

   function Long_Random(S : in String)
      return U64
      is
      Dummy : U64 := A_Prime;
      begin
      Loop_On_Every_Character :
      for W in S'Range loop
         Dummy := Dummy + Character'Pos(S(W));
         Dummy := Dummy xor Dummy * 2 ** 21;
         Dummy := Dummy xor Dummy / 2 ** 35;
         Dummy := Dummy xor Dummy * 2 ** 4;
      end loop Loop_On_Every_Character;
      return Dummy;
   end Long_Random;

   -------------
   --  NHASH  --
   -------------

   function Nhash(Message : in String)
      return U64
      is
      type Mod_257 is mod 257;-- PRIME
      An_Index : Mod_257 := 251;-- PRIME
      -- RANDOM 64 BITS IN A ROUND ROBIN ARRAY (PRIME MODULO INDICE)
      Round_Robin_Array : constant array (Mod_257'Range) of U64 :=
      (16#3A62C1D3160E9A32#, 16#3362D984573059B2#, 16#346ADD9B06AE6132#, 16#C260DA0C56CC59B2#,
       16#F7BD7B19610B7D5A#, 16#5A9C04FC64B8AFBD#, 16#28C91A9B87703029#, 16#8992FF482C98A4F4#,
       16#82D48BECFCE8C8C8#, 16#44191457EF74B393#, 16#159D9B21B772DAE5#, 16#D727C8C857E0279F#,
       16#095DB18D43D43D72#, 16#0C9856F0E5A837A2#, 16#06CB88DB5E7FAB42#, 16#0A75BEBA0C8D4072#,
       16#01000A01003A3432#, 16#0000030103343332#, 16#2843030403333332#, 16#0003030300A5C87B#,
       16#725DBB6934CC41F3#, 16#8C68F60FD5DABEA9#, 16#4673CE9092722F4C#, 16#D9BF634DCF1CDA30#,
       16#0B57633DA7BB1A7C#, 16#8FF4034A96167CD1#, 16#2823C1A55E5273A2#, 16#0BAB905A25D08C72#,
       16#4F5B85C066377AA7#, 16#18E3EA8316CCD3EF#, 16#9DEB15C709386563#, 16#A3C3693DBC89A258#,
       16#13F87B4B17B420DC#, 16#1E20FE71456B056F#, 16#7D9A44CF3303FD36#, 16#844949E3B721002F#,
       16#89456E2A83043FE4#, 16#7D8A31319D42C120#, 16#A158CC1DC7D3CCC6#, 16#2ECA3F0547FBAD9B#,
       16#F8E540B6B0F4627A#, 16#470129FE879AEAD0#, 16#5A84E0BA5E9FA250#, 16#0C1BA9CF54B093DA#,
       16#EC537CD396DCA921#, 16#147755219CC0415B#, 16#6DBB4D32A75B6245#, 16#E6F18812F8DE641A#,
       16#57E8338CBB419DFA#, 16#C358071D9EC038D6#, 16#CB50075D9E803AD4#, 16#C8AC074261603A2B#,
       16#2A2C8F33FB419DFA#, 16#21F2B8A5C89034F8#, 16#5F8C6A2B8A403672#, 16#2827F5B64EC03056#,
       16#DDD9BE20C71BBF2F#, 16#DBDA8DE94954CA58#, 16#E300F931EAF60E6D#, 16#76625BBD67AF3EAB#,
       16#889308093C930C78#, 16#7D4277747E028B67#, 16#EBF5DF86C1568658#, 16#08CE3053400820D9#,
       16#E8DCF9AA0CDEBD34#, 16#0DB2AEFD53621B7F#, 16#0DC4A62073196E53#, 16#78FACE607C478EA9#,
       16#3A62C1D3162E9A39#, 16#3362D994573059B9#, 16#B46ADD9B06AE6139#, 16#C260DA0C56CC59B9#,
       16#F7BD7B19610BFD51#, 16#5A9C04FC64802FCD#, 16#28C91A9B73C9B009#, 16#8992FF31C43024AD#,
       16#FD1E989676393762#, 16#A056DA6955042F92#, 16#7D97C540943EA3C0#, 16#44F6C1DEF03D9798#,
       16#095DB18D43D44D79#, 16#5F8A80CB7C750FB1#, 16#18F6A8C6AB20C8A9#, 16#0A75BEBF4B276579#,
       16#01000A01013A3439#, 16#0001030103343339#, 16#0003030403333339#, 16#0003030303333339#,
       16#725DBB6934CCCBE9#, 16#8C68F60FD51B94E8#, 16#4673CE9BED076F4B#, 16#D9B393EC296288A0#,
       16#15240B35BE59975C#, 16#2481BDBA2BE4DBA6#, 16#5EEAC2196E29E879#, 16#5AD580556EDCB103#,
       16#4E6F32601A34EA4D#, 16#3CF923639E200FB9#, 16#B479ED5396848A07#, 16#CFAA23EB3E85E5AE#,
       16#13F87B4B17B5BDB6#, 16#1E20FE7145A7B859#, 16#7D9A44CFB3B5BC77#, 16#8449459EDC5B904C#,
       16#89466EE7B18B10AA#, 16#DC5DA5E406FE35BB#, 16#48A389963B16DBF7#, 16#2A406FA06FD9BDF6#,
       16#17AC7BB6B6166426#, 16#C1B3133FB752EA19#, 16#7AF8C9A3EFD098D7#, 16#EC1FDFAB19F9952#,
       16#EC537CD396DCA921#, 16#147755219CC0415B#, 16#6DBB4D32A75B6245#, 16#E6F18812F8DE641A#,
       16#59083315F7419E42#, 16#CAC8075152403AB2#, 16#F278069517C0349E#, 16#F368069D9F4034DA#,
       16#2AF0C2338C419E42#, 16#2F8C6A8C846037FB#, 16#9A5C2B8C3F603553#, 16#F8A28CD1A2E03737#,
       16#DDD9BE21087BD96B#, 16#DBDA8DD967A9D918#, 16#E300F934D6593665#, 16#76625BACCBFFFA0E#,
       16#8A4FA6D405EEA037#, 16#1F115D76211DDD7B#, 16#6B00823AE2250EA8#, 16#A420277224022B91#,
       16#67F08339E076C11A#, 16#2C44907EF05F97E6#, 16#5CCD333F3F4546DB#, 16#916471ACA03AF4C2#,
       16#3A62C1D3164E98B6#, 16#3362D9A4572E2136#, 16#B46ADD94564C59B6#, 16#B46AD9A436AE6136#,
       16#F7BD7B19610A7DFE#, 16#5A9C04FC6488A1D9#, 16#28C91A9B7371EC94#, 16#8992FF312CB26980#,
       16#F5CCA58D8AF8AD71#, 16#2FFAC3ABCCC0A14E#, 16#339CDB741A9E6461#, 16#4AEAAC8689A21C35#,
       16#095DB18D43D45D46#, 16#0C9856FC8E98C216#, 16#06B8A95F2C8BCA86#, 16#0A75BEB9F58C7546#,
       16#01000A01023A3136#, 16#0F8B55C9A5C8FB56#, 16#1A85B9FC81AB0C86#, 16#9F58A6B8C05D9ED6#,
       16#725DBB6934CD4AD9#, 16#8C68F60FD4A32A10#, 16#4673CE9A62225BA7#, 16#D9B3AA52F7D7DD9E#,
       16#5530C36AB465C8E5#, 16#A4F11CA4878C5427#, 16#B799420670E7B452#, 16#3ABEFC66D7B15016#,
       16#5AEF0664E298AD13#, 16#2F8EDA41DA9EA577#, 16#46410A395BD78AB1#, 16#56AF03690B24FD66#,
       16#13F87B4B17B5287F#, 16#1E20FE7145A02230#, 16#7D9A44CFB428C985#, 16#8449459FEF070D40#,
       16#89476FA4E00EE0A9#, 16#D138C76BCB6B65E9#, 16#447A0B72A1C67C6F#, 16#9CC0A59D4B6B92D5#,
       16#FCA007B6A6772FE3#, 16#1F34FACE298C64C3#, 16#CFC611E458F0E9EE#, 16#82DA1B8F629585F5#,
       16#EC537CD396DCA921#, 16#147755219CC0415B#, 16#6DBB4D32A75B6245#, 16#E6F18812F8DE641A#,
       16#5634338255A19D8D#, 16#F058068406C03416#, 16#FBAC06DBF96036EB#, 16#DC3007E625803F0C#,
       16#2D8C8A53F5A19D8D#, 16#21F8D2C6F3A035BD#, 16#F8C5D8A991E036AF#, 16#7AB8D8C682C03636#,
       16#DDD9BE201FDE7FEC#, 16#DBDA8DF6B3A03BA7#, 16#E300F93235089113#, 16#76625B9FB27DCA76#,
       16#4CC9A9D0D28BE695#, 16#EA9E4082AA1CD881#, 16#0CF508EB8EA8BBCB#, 16#3A44332D094D7A20#,
       16#463516B66434375A#, 16#D122BCC57067BA29#, 16#FA2E0BA6B2D9C25C#, 16#7F2191E5FB389DD3#,
       16#3A62C1D3164E99B3#, 16#3362D9A4572E6133#, 16#B46ADD9456CC59B3#, 16#B46ADA1C36AE6133#,
       16#F7BD7B19610A7DBB#, 16#5A9C04FC6488A15E#, 16#28C91A9B7371E2C8#, 16#8992FF312CB3A2CC#,
       16#4C34A559CE03FDC4#, 16#651DF9A7D83D42DF#, 16#95958663A051898B#, 16#50E5869AD73DBD50#,
       16#095DB18D43D45D63#, 16#0C9856FA8B8D9793#, 16#06CB8DF8CFDACAC3#, 16#0A75BEB0D2A87563#,
       16#01000A01023ACDE3#, 16#85402030101F8D5C#, 16#D8C58A94D8C9D4A8#, 16#A85F8B9D8C91D9A5#,
       16#725DBB6934CD4D97#, 16#8C68F60FD4A01EE5#, 16#4673CE9A65E973D6#, 16#D9B3AA3426DD8606#,
       16#31060DECCA1CA5C0#, 16#E59BC0653E0EAC0E#, 16#63D72A2CE16ACFC8#, 16#7EB7AD66CA202710#,
       16#576F1A79427F38CA#, 16#AC0E8F4074908FF1#, 16#2F1C521EBB9F47F7#, 16#5761E03327E0613E#,
       16#13F87B4B17B528B8#, 16#1E20FE7145A0241B#, 16#7D9A44CFB428B033#, 16#8449459FEF088D05#,
       16#89476FA4E010E124#, 16#57B550D64FA307DE#, 16#A56981887F18B22D#, 16#91139EE7566AB1C7#,
       16#FCA005B6A6772C5C#, 16#74AFADCB10535324#, 16#6A4A5DAF1D8294A0#, 16#A3EFE4A10E14DC6F#,
       16#492EB91D8BEF7335#, 16#5DCD99B6222D1A5F#, 16#D4E70AB48CA719BC#, 16#DC98C89F45D8CABB#,
       16#E82A14B5083F6E75#, 16#4B2A1F133051DC43#, 16#64F15CEDBE5D170B#, 16#71037259748EDD93#,
       16#20000033FB419DFA#);

      Sum : U64 := A_Prime;
      M_1 : U64 := 0;
      M_2 : U64 := 0;
      begin
      Loop_On_One_Out_Of_Two_Characters :
      for I in 1 .. U64(Message'Length / 2 - 1) loop
         M_1      := U64(Character'Pos(Message(2       * Natural(I))));
         M_2      := U64(Character'Pos(Message(1 + 2   * Natural(I))));
         Sum      := Sum                         + ((M_1 + Round_Robin_Array(2 * An_Index)) * (M_2 + Round_Robin_Array(2 * An_Index + 1)));
         An_Index := 11                          + An_Index;-- 11 IS  PRIME
      end loop Loop_On_One_Out_Of_Two_Characters;
      return Sum;
   end Nhash;

   ---------------
   --  SIPHASH  --
   ---------------

   function Siphash(Message : in String)
      return U64
      is
      -- 8 CHARACTERS INTO WORD
      subtype String_8 is String(1 .. 8);
      function S_To_W is new Unchecked_Conversion(Source => String_8, Target => U64);

      Key : constant String(1 .. 16) := "0123456789ABCDEF";
      -- EASIER CODING
      type V_Type is array (0 .. 3) of U64;
      V : V_Type := (others=> 0);

      Mi, K0, K1, Block, Last7, Result : U64;
      Dummy                            : U64 := 0;
      I                                : Natural;
      Len                              : constant Natural := Message'Length;
      --
      --   LOCAL INDIAN_CHANGE_64
      --
      function Indian_Change_64(W : in U64)
         return U64
         is
         Res              : U64          := 0;
         Byte_7_To_Byte_0 : constant U64 := 16#00000000000000FF#;
         Byte_6_To_Byte_1 : constant U64 := 16#000000000000FF00#;
         Byte_5_To_Byte_2 : constant U64 := 16#0000000000FF0000#;
         Byte_4_To_Byte_3 : constant U64 := 16#00000000FF000000#;
         Byte_3_To_Byte_4 : constant U64 := 16#000000FF00000000#;
         Byte_2_To_Byte_5 : constant U64 := 16#0000FF0000000000#;
         Byte_1_To_Byte_6 : constant U64 := 16#00FF000000000000#;
         Byte_0_To_Byte_7 : constant U64 := 16#FF00000000000000#;
         begin
         Res :=
         ((W    / 2 ** ((7 - 0) * 8))                 and Byte_7_To_Byte_0)
         or ((W / 2 ** ((6 - 1) * 8))                 and Byte_6_To_Byte_1)
         or ((W / 2 ** ((5 - 2) * 8))                 and Byte_5_To_Byte_2)
         or ((W / 2 ** ((4 - 3) * 8))                 and Byte_4_To_Byte_3)
         or ((W                 * 2 ** ((4 - 3) * 8)) and Byte_3_To_Byte_4)
         or ((W                 * 2 ** ((5 - 2) * 8)) and Byte_2_To_Byte_5)
         or ((W                 * 2 ** ((6 - 1) * 8)) and Byte_1_To_Byte_6)
         or ((W                 * 2 ** ((7 - 0) * 8)) and Byte_0_To_Byte_7);
         return Res;
      end Indian_Change_64;
      --|
      --|SIPCOMPRESS
      --|
      procedure Sipcompress(V : in out V_Type)
         is
         begin
         V(0) := V(0) + V(1);
         V(2) := V(2) + V(3);
         V(1) := Left_Rotation(V(1), 13);
         V(3) := Left_Rotation(V(3), 16);
         V(1) := V(1) xor V(0);
         V(3) := V(3) xor V(2);
         V(0) := Left_Rotation(V(0), 32);
         V(2) := V(2) + V(1);
         V(0) := V(0) + V(3);
         V(1) := Left_Rotation(V(1), 17);
         V(3) := Left_Rotation(V(3), 21);
         V(1) := V(1) xor V(2);
         V(3) := V(3) xor V(0);
         V(2) := Left_Rotation(V(2), 32);
      end Sipcompress;
      --|              |-=-=-=-=-=-=-=-=-=-=-=-=-=-|
      --|              |  M A I N    S I P H A S H |
      --|              |-=-=-=-=-=-=-=-=-=-=-=-=-=-|
      begin
      -- TRANSLATE U8TO64_LE(KEY + 0);
      K0 := S_To_W(Key(1 .. 8));
      K0 := Indian_Change_64(K0);

      -- TRANSLATE U8TO64_LE(KEY + 8);
      K1 := S_To_W(Key(9 .. 16));
      K1 := Indian_Change_64(K1);

      V(0) := K0 xor 16#736F6D6570736575#;
      V(1) := K1 xor 16#646F72616E646F6D#;
      V(2) := K0 xor 16#6C7967656E657261#;
      V(3) := K1 xor 16#7465646279746573#;

      Last7 := (U64(Len) and 16#FF#) * 2 ** 56;
      Block := U64(Len)  and not (7);
      I     := 0;

      Loop_On_Every_Character :
      while U64(I) < Block loop
         Mi   := S_To_W(Message(1 + I .. 8 + I));
         Mi   := Indian_Change_64(Mi);
         V(3) := V(3) xor Mi;
         Sipcompress(V);
         Sipcompress(V);
         V(0) := V(0) xor Mi;
         I    := 8 + I;
      end loop Loop_On_Every_Character;

      case U64(Len) - Block -- 1 + I  BECAUSE STRINGS START AT 1 IN ADA NOT 0
         is
         when 7     => Last7 := Last7 or (U64(Character'Pos(Message(1 + I + 6))) * 2 ** 48);
         when 6     => Last7 := Last7 or (U64(Character'Pos(Message(1 + I + 5))) * 2 ** 40);
         when 5     => Last7 := Last7 or (U64(Character'Pos(Message(1 + I + 4))) * 2 ** 32);
         when 4     => Last7 := Last7 or (U64(Character'Pos(Message(1 + I + 3))) * 2 ** 24);
         when 3     => Last7 := Last7 or (U64(Character'Pos(Message(1 + I + 2))) * 2 ** 16);
         when 2     => Last7 := Last7 or (U64(Character'Pos(Message(1 + I + 1))) * 2 ** 08);
         when 1     => Last7 := Last7 or U64(Character'Pos(Message(1  + I + 0)));
         when others=> null;
      end case;

      V(3) := V(3) xor Last7;
      Sipcompress(V);
      Sipcompress(V);
      V(0) := V(0) xor Last7;
      V(2) := V(2) xor 16#FF#;
      Sipcompress(V);
      Sipcompress(V);
      Sipcompress(V);
      Sipcompress(V);

      Result := V(0) xor V(1) xor V(2) xor V(3);

      return Result;
   end Siphash;

   -----------------------
   --  RIPEMD_320_HASH  --
   -----------------------

   procedure Ripemd_320_Hash(Message                                : in String;
                             H0, H1, H2, H3, H4, H5, H6, H7, H8, H9 : out U32)
      is
      function Rol(A : in U32;
                   B : in U32)
         return U32
         is
         Exp : constant Natural := Natural(B);
         begin
         return (A * 2 ** Exp) or (A / 2 ** (32 - Exp));
      end Rol;
      --|
      --| CREATE A VOID TYPE BECAUSE THE MESSAGE LENGTH IS NOT KNOWN AT THIS STAGE
      --|
      type Word_Message_Type is array (Natural range <>, Natural range <>) of U32;

      function F(J : in Natural;
                 X : in U32;
                 Y : in U32;
                 Z : in U32)
         return U32
         is
         Result : U32 := 0;
         begin
         case J is
            when 0 .. 15 =>
            Result := X xor Y xor Z;
            when 16 .. 31 =>
            Result := (X and Y) or ((not X) and Z);
            when 32 .. 47 =>
            Result := (X or not (Y)) xor Z;
            when 48 .. 63 =>
            Result := (X and Z) or (Y and (not Z));
            when 64 .. 79 =>
            Result := X xor (Y or (not Z));
            when others=>
            null;
         end case;
         return Result;
      end F;
      --|
      --| MD4_PADDING_OF_THE_MESSAGE
      --|
      function Md4_Padding_Of_The_Message(The_Message : in String)
         return Word_Message_Type
         is
         Original_Message_Length : Natural := 0;
         Extended_Message_Length : Natural := 0;
         Number_Of_16_Word_Block : Natural := 0;
         Slice_Beginning         : Natural := 0;
         --|
         --| 4 CHARACTERS STRING BACK AND FORTH INTO 32 BITS WORD
         --|
         subtype String_4 is String(1 .. 4);
         Strg_4     : String_4 := (others=> Ascii.Nul);
         Strg4_Word : U32      := 0;
         function S_To_W_4 is new Unchecked_Conversion(Source => String_4, Target => U32);
         function W_To_S_4 is new Unchecked_Conversion(Source => U32     , Target => String_4);
         --|
         --| 8 CHARACTERS STRING BACK AND FORTH INTO INTO  64 BITS WORD_64
         --|
         --|TYPE WORD_64 IS MOD 2 ** 64;
         subtype String_8 is String(1 .. 8);
         Strg_8     : String_8 := (others=> Ascii.Nul);
         Strg8_Word : U64      := 0;
         --FUNCTION S_TO_W_8 IS NEW UNCHECKED_CONVERSION(SOURCE => STRING_8, TARGET => WORD_64);
         function W_To_S_8 is new Unchecked_Conversion(Source => U64, Target => String_8);

         begin

         Original_Message_Length := The_Message'Length;

         -- 512/8 =  64 IN CHARACTERS
         Extended_Message_Length := Original_Message_Length / 64 * 64 + 64;
         Number_Of_16_Word_Block := Extended_Message_Length / 64;

         Message_Padding :
         declare
         --
         -- FIRST FILL IT ALL WITH ZEROS AS SPECIFIED IN MD4
         --
         Padded_String : String(1 .. Extended_Message_Length) := (others=> Ascii.Nul);
         --
         -- CREATE THE PADDED MESSAGE BLOCKS: NOW WE KNOW THE DETAILS!
         --
         Padded_Message : Word_Message_Type(0 .. Number_Of_16_Word_Block - 1, 0 .. 15);

         begin
            --
            --INSERT THE ORIGINAL MESSAGE
            --
            Padded_String(1 .. Original_Message_Length) := The_Message;
            --|
            --|          A SINGLE "1" BIT IS APPENDED
            --|

            -- STRING TO WORD
            Strg4_Word := S_To_W_4(Padded_String(Original_Message_Length + 1 .. Original_Message_Length + 4));
            -- PUT THE LEFTMOST BIT ONE TO ONE
            Strg4_Word := Strg4_Word and 2 ** 31;
            -- BACK TO STRING OF 4 CHAR
            Strg_4 := W_To_S_4(Strg4_Word);
            -- BACK IN PADDED_STRING
            Padded_String(Original_Message_Length + 1 .. Original_Message_Length + 4) := Strg_4;
            --
            --                           APPEND LENGTH
            --

            --    PUT VALUE INTO A 64 BITS WORD
            Strg8_Word := U64(Original_Message_Length * 8);
            -- BACK TO STRING OF 8 CHAR
            Strg_8 := W_To_S_8(Strg8_Word);
            -- BACK IN PADDED_STRING
            Padded_String(Original_Message_Length + 5 .. Original_Message_Length + 12) := Strg_8;
            --
            --TO CUT INTO CHAR
            --
            Slice_Beginning := 1;

            Loop_On_How_Many_Blocks :
            for H in 0 .. Number_Of_16_Word_Block - 1 loop

               Loop_On_Block_Size :
               for K in 0 .. 15 loop
                  --CURRENT SLICE OF 4 CHARACTERS INTO A WORD
                  Strg4_Word := S_To_W_4(Padded_String(Slice_Beginning .. Slice_Beginning + 3));
                  -- ADD TO PADDED_MESSAGE
                  Padded_Message(H, K) := Strg4_Word;
                  -- FOR NEXT SLICE
                  Slice_Beginning := 4 + Slice_Beginning;

               end loop Loop_On_Block_Size;
            end loop Loop_On_How_Many_Blocks;
            return Padded_Message;
         end Message_Padding;
      end Md4_Padding_Of_The_Message;
      --|
      --|
      --|    ADDED CONSTANTS (HEXADECIMAL)
      --|
      K : constant array (0 .. 79) of U32 :=
      (0 .. 15  => 16#00000000#,
       16 .. 31 => 16#5A827999#,
       32 .. 47 => 16#6ED9EBA1#,
       48 .. 63 => 16#8F1BBCDC#,
       64 .. 79 => 16#A953FD4E#);
      --|
      --|    ADDED CONSTANTS (HEXADECIMAL)
      --|
      K_Prime : constant array (0 .. 79) of U32 :=
      (0 .. 15  => 16#50A28BE6#,
       16 .. 31 => 16#5C4DD124#,
       32 .. 47 => 16#6D703EF3#,
       48 .. 63 => 16#7A6D76E9#,
       64 .. 79 => 16#00000000#);
      --|
      --|    SELECTION OF  ROTATION :
      --|
      R : constant array (0 .. 79) of Natural :=
      (00, 01, 02, 03, 04, 05, 06, 07, 08, 09, 10, 11, 12, 13, 14, 15,
       07, 04, 13, 01, 10, 06, 15, 03, 12, 00, 09, 05, 02, 14, 11, 08,
       03, 10, 14, 04, 09, 15, 08, 01, 02, 07, 00, 06, 13, 11, 05, 12,
       01, 09, 11, 10, 00, 08, 12, 04, 13, 03, 07, 15, 14, 05, 06, 02,
       04, 00, 05, 09, 07, 12, 02, 10, 14, 01, 03, 08, 11, 06, 15, 13);
      --|
      --|    SELECTION OF  ROTATION :
      --|
      R_Prime : constant array (0 .. 79) of Natural :=
      (5 , 14, 7, 0, 9, 2, 11, 4, 13, 6, 15, 8, 1, 10, 3, 12,
       6 , 11, 3, 7, 0, 13, 5, 10, 14, 15, 8, 12, 4, 9, 1, 2,
       15, 5, 1, 3, 7, 14, 6, 9, 11, 8, 12, 2, 10, 0, 4, 13,
       8 , 6, 4, 1, 3, 11, 15, 0, 5, 12, 2, 13, 9, 7, 10, 14,
       12, 15, 10, 4, 1, 5, 8, 7, 6, 2, 13, 14, 0, 3, 9, 11);
      --|
      --|    SELECTION OF  INDEX DRIVEN DATA :
      --|
      S : constant array (0 .. 79) of U32 :=
      (11, 14, 15, 12, 5, 8, 7, 9, 11, 13, 14, 15, 6, 7, 9, 8,
       7 , 6, 8, 13, 11, 9, 7, 15, 7, 12, 15, 9, 11, 7, 13, 12,
       11, 13, 6, 7, 14, 9, 13, 15, 14, 8, 13, 6, 5, 12, 7, 5,
       11, 12, 14, 15, 14, 15, 9, 8, 9, 14, 5, 6, 8, 6, 5, 12,
       9 , 15, 5, 11, 6, 8, 13, 12, 5, 12, 13, 14, 11, 8, 5, 6);
      --|
      --|    SELECTION OF  INDEX DRIVEN DATA :
      --|
      S_Prime : constant array (0 .. 79) of U32 :=
      (8 , 9, 9, 11, 13, 15, 15, 5, 7, 7, 8, 11, 14, 14, 12, 6,
       9 , 13, 15, 7, 12, 8, 9, 11, 7, 7, 12, 7, 6, 15, 13, 11,
       9 , 7, 15, 11, 8, 6, 6, 14, 12, 13, 5, 14, 13, 13, 7, 5,
       15, 5, 8, 11, 14, 14, 6, 14, 6, 9, 12, 9, 12, 5, 15, 8,
       8 , 5, 12, 9, 12, 5, 14, 6, 8, 13, 6, 5, 15, 13, 11, 11);

      T : U32 := 0;

      Number_Of_16_Word_Block   : Natural   := 0;-- NUMBER OF 16 WORD BLOCK IN THE MESSAGE
      Extended_Message_Length   : Natural   := 0;
      A      , B, C, D, E             : U32 := 0;
      A_Prime, B_Prime          : U32       := 0;
      C_Prime, D_Prime, E_Prime : U32       := 0;
      --
      --  GET THE MESSAGE AFTER PADDING
      --  =============================
      --
      X : constant Word_Message_Type := Md4_Padding_Of_The_Message(The_Message => Message);
      --
      -- HASH THE MESSAGE AFTER PADDING
      --
      begin
      --INITIAL VALUES  HEXADECIMAL
      H0 := 16#67452301#;
      H1 := 16#EFCDAB89#;
      H2 := 16#98BADCFE#;
      H3 := 16#10325476#;
      H4 := 16#C3D2E1F0#;

      H5 := 16#76543210#;
      H6 := 16#FEDCBA98#;
      H7 := 16#89ABCDEF#;
      H8 := 16#01234567#;
      H9 := 16#3C2D1E0F#;
      --
      -- GET NUMBER_OF_16_WORD_BLOCK  FOR THE LOOP
      --
      Extended_Message_Length := Message'Length          / 64 * 64 + 64;
      Number_Of_16_Word_Block := Extended_Message_Length / 64;
      --
      --  LOOP ON BLOCKS
      --
      Loop_On_Blocks :
      for I in 00 .. Number_Of_16_Word_Block - 1 loop

         E       := H4;
         D       := H3;
         C       := H2;
         B       := H1;
         A       := H0;
         E_Prime := H9;
         D_Prime := H8;
         C_Prime := H7;
         B_Prime := H6;
         A_Prime := H5;
         --
         -- 79 ITERATIONS
         --
         Loop_On_79_Iterations :
         for J in 00 .. 79 loop

            T := Rol((A + F(J, B, C, D) + X(I, R(J)) + K(J)), S(J)) + E;

            A := E;
            E := D;
            D := Rol(C, 10);
            C := B;
            B := T;

            T := Rol((A_Prime + F(79 - J, B_Prime, C_Prime, D_Prime)
                              + X(I     , R_Prime(J)) + K_Prime(J)), S_Prime(J)) + E_Prime;

            A_Prime := E_Prime;
            E_Prime := D_Prime;
            D_Prime := Rol(C_Prime, 10);
            C_Prime := B_Prime;
            B_Prime := T;

            case J is
               when 15 =>
               T       := B;
               B       := B_Prime;
               B_Prime := T;
               when 31 =>
               T       := D;
               D       := D_Prime;
               D_Prime := T;
               when 47 =>
               T       := A;
               A       := A_Prime;
               A_Prime := T;
               when 63 =>
               T       := C;
               C       := C_Prime;
               C_Prime := T;
               when 79 =>
               T       := E;
               E       := E_Prime;
               E_Prime := T;
               when others=> null;
            end case;
         end loop Loop_On_79_Iterations;

         H0 := H0 + A;
         H1 := H1 + B;
         H2 := H2 + C;
         H3 := H3 + D;
         H4 := H4 + E;
         H5 := H5 + A_Prime;
         H6 := H6 + B_Prime;
         H7 := H7 + C_Prime;
         H8 := H8 + D_Prime;
         H9 := H9 + E_Prime;
      end loop Loop_On_Blocks;
   end Ripemd_320_Hash;

   --------------------
   --  BALLOON_HASH  --
   --------------------

   function Balloon_Hash(Message : in String;
                         Salt    : in String)
      return U64
      is
      --|
      --|AUXILLIARY HASH FUNCTION
      --|
      function Rshash(S : in String)
         return U64
         is
         Char_Value : U64          := 0;
         Hash       : U64          := A_Prime;
         B          : constant U64 := 378551;
         A          : U64          := 63689;
         begin

         Loop_On_Every_Character :
         for K in S'Range loop
            Char_Value := Character'Pos(S(K));
            Hash       := Hash * A + Char_Value;
            A          := A    * B;
         end loop Loop_On_Every_Character;

         return Hash;
      end Rshash;
      --|
      --|     M A I N
      --|
      S_Cost    : constant Natural           := 100;
      T_Cost    : constant Natural           := 10;
      Delt      : constant Natural           := 5;
      Cnt       : Natural                    := 0;
      Buf       : array (0 .. S_Cost) of U64 := (others=> 0);
      Prev      : U64                        := 0;
      Idx_Block : U64                        := 0;
      Oth       : Integer                    := 0;
      begin
      --|
      --| FILL UP BUFFER WITH 'RANDOM' VALUES
      --|
      Buf(0) := Rshash(Natural'Image(Cnt) & Message & Salt);
      for M in 1 .. S_Cost - 1 loop
         Cnt    := 1 + Cnt;
         Buf(M) := Rshash(Natural'Image(Cnt) & U64'Image(Buf(M - 1)));
      end loop;
      --|
      --|HASH
      --|
      for T in 0 .. T_Cost    - 1 loop
         for M in 0 .. S_Cost - 1 loop
            Prev   := Buf((M  - 1) mod S_Cost);
            Buf(M) := Rshash(Natural'Image(Cnt) & U64'Image(Prev) & U64'Image(Buf(M)));
            Cnt    := 1 + Cnt;
            for I in 0 .. Delt - 1 loop
               Idx_Block := Rshash(Natural'Image(M) & Natural'Image(T) & Natural'Image(I));--
               Oth       := Integer(Rshash(Natural'Image(Cnt) & Salt & U64'Image(Idx_Block)) mod U64(S_Cost));
               Cnt       := 1 + Cnt;
               Buf(M)    := Rshash(Natural'Image(Cnt) & U64'Image(Buf(M)) & U64'Image(Buf(Oth)));
               Cnt       := 1 + Cnt;
            end loop;
         end loop;
      end loop;
      return Buf(S_Cost - 1);
   end Balloon_Hash;



   -- TEST ALL

   Result   : U64;
   How_Many : constant Natural := 8;


   -- EXEMPLE OF DUMMY STRING FOR THE DEMONSTRATION
   Strg : constant String(1 .. 32) := ("12345678901234567890123456789012");
   begin

   Atio.New_Line(Spacing => 3);
   Atio.Put_Line
   (Ascii.Lf
    & "                               =-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=");
   Atio.Put_Line
   ("                                H A S H   T E S T S  O N   R A N D O M   V A L U E S");
   Atio.Put_Line
   ("                               =-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=");
   Atio.New_Line(Spacing => 3);
   --|
   --|              |_____________________________|
   --|              |                             |
   --|              |        TEST  BPHASH         |
   --|              |_____________________________|
   --|              |                             |
   --|
   --INITIALIZE
   Result := Bphash(S => Strg);
   Atio.Put("BPHASH        " & "16#" & Print_Integer(Result, 16, 16) & "#,");

   -- SEND THE RESULT BACK AS A STRING FOR THE NEXT_TEST
   for W in 1 .. How_Many - 1 loop
      Result := Bphash(S => Print_Integer(Result, 16, 16));
      Atio.Put("16#" & Print_Integer(Result     , 16, 16) & "#,");
   end loop;

   Atio.New_Line;
   --|
   --|              |_______________|
   --|              |               |
   --|              | TEST  DEKHASH |
   --|              |_______________|
   --|              |               |
   --|
   --INITIALIZE
   Result := Dekhash(S => Strg);
   Atio.Put("DEKHASH       " & "16#" & Print_Integer(Result, 16, 16) & "#,");

   -- SEND THE RESULT BACK AS A STRING FOR THE NEXT_TEST
   for W in 1 .. How_Many - 1 loop

      Result := Dekhash(S => Print_Integer(Result, 16, 16));
      Atio.Put("16#" & Print_Integer(Result      , 16, 16) & "#,");

   end loop;

   Atio.New_Line;
   --|
   --|              |________________|
   --|              |                |
   --|              | TEST BKDRHASH  |
   --|              |________________|
   --|              |                |
   --|
   --INITIALIZE
   Result := Bkdrhash(S => Strg);
   Atio.Put("BKDRHASH      " & "16#" & Print_Integer(Result, 16, 16) & "#,");

   -- SEND THE RESULT BACK AS A STRING FOR THE NEXT_TEST
   for W in 1 .. How_Many - 1 loop

      Result := Bkdrhash(S => Print_Integer(Result, 16, 16));
      Atio.Put("16#" & Print_Integer(Result       , 16, 16) & "#,");

   end loop;

   Atio.New_Line;
   --|
   --|              |________________|
   --|              |                |
   --|              | TEST   ELFHASH |
   --|              |________________|
   --|              |                |
   --|
   --INITIALIZE
   Result := Elfhash(S => Strg);
   Atio.Put("ELFHASH       " & "16#" & Print_Integer(Result, 16, 16) & "#,");

   -- SEND THE RESULT BACK AS A STRING FOR THE NEXT_TEST
   for W in 1 .. How_Many - 1 loop

      Result := Elfhash(S => Print_Integer(Result, 16, 16));
      Atio.Put("16#" & Print_Integer(Result      , 16, 16) & "#,");

   end loop;

   Atio.New_Line;
   --|
   --|              |______________|
   --|              |              |
   --|              | TEST PJWHASH |
   --|              |______________|
   --|              |              |
   --|
   --INITIALIZE
   Result := Pjwhash(S => Strg);
   Atio.Put("PJWHASH       " & "16#" & Print_Integer(Result, 16, 16) & "#,");

   -- SEND THE RESULT BACK AS A STRING FOR THE NEXT_TEST
   for W in 1 .. How_Many - 1 loop

      Result := Pjwhash(S => Print_Integer(Result, 16, 16));
      Atio.Put("16#" & Print_Integer(Result      , 16, 16) & "#,");

   end loop;

   Atio.New_Line;
   --|
   --|              |_____________|
   --|              |             |
   --|              | TEST JSHASH |
   --|              |_____________|
   --|              |             |
   --|
   --INITIALIZE
   Result := Jshash(S => Strg);
   Atio.Put("JSHASH        " & "16#" & Print_Integer(Result, 16, 16) & "#,");

   -- SEND THE RESULT BACK AS A STRING FOR THE NEXT_TEST
   for W in 1 .. How_Many - 1 loop

      Result := Jshash(S => Print_Integer(Result, 16, 16));
      Atio.Put("16#" & Print_Integer(Result     , 16, 16) & "#,");

   end loop;

   Atio.New_Line;
   --|
   --|              |_____________|
   --|              |             |
   --|              | TEST RSHASH |
   --|              |_____________|
   --|              |             |
   --|
   --INITIALIZE
   Result := Rshash(S => Strg);
   Atio.Put("RSHASH        " & "16#" & Print_Integer(Result, 16, 16) & "#,");

   for W in 1 .. How_Many - 1 loop

      -- SEND THE RESULT BACK AS A STRING FOR THE NEXT_TEST
      Result := Rshash(S => Print_Integer(Result, 16, 16));
      Atio.Put("16#" & Print_Integer(Result     , 16, 16) & "#,");

   end loop;

   Atio.New_Line;
   --|
   --|              |_____________|
   --|              |             |
   --|              | TEST APHASH |
   --|              |_____________|
   --|              |             |
   --|
   --INITIALIZE
   Result := Aphash(S => Strg);
   Atio.Put("APHASH        " & "16#" & Print_Integer(Result, 16, 16) & "#,");

   for W in 1 .. How_Many - 1 loop

      -- SEND THE RESULT BACK AS A STRING FOR THE NEXT_TEST
      Result := Aphash(S => Print_Integer(Result, 16, 16));
      Atio.Put("16#" & Print_Integer(Result     , 16, 16) & "#,");

   end loop;

   Atio.New_Line;
   --|
   --|              |_____________|
   --|              |             |
   --|              | TEST   DJB2 |
   --|              |_____________|
   --|              |             |
   --|
   --INITIALIZE
   Result := Djb2(S => Strg);
   Atio.Put("DJB2          " & "16#" & Print_Integer(Result, 16, 16) & "#,");

   for W in 1 .. How_Many - 1 loop

      -- SEND THE RESULT BACK AS A STRING FOR THE NEXT_TEST
      Result := Djb2(S => Print_Integer(Result, 16, 16));
      Atio.Put("16#" & Print_Integer(Result   , 16, 16) & "#,");

   end loop;

   Atio.New_Line;
   --|
   --|              |__________|
   --|              |          |
   --|              | TEST SDBM|
   --|              |__________|
   --|              |          |
   --|
   --INITIALIZE
   Result := Sdbm(S => Strg);
   Atio.Put("SDBM          " & "16#" & Print_Integer(Result, 16, 16) & "#,");

   for W in 1 .. How_Many - 1 loop

      -- SEND THE RESULT BACK AS A STRING FOR THE NEXT_TEST
      Result := Sdbm(S => Print_Integer(Result, 16, 16));
      Atio.Put("16#" & Print_Integer(Result   , 16, 16) & "#,");

   end loop;

   Atio.New_Line;
   --|
   --|              |_____________|
   --|              |             |
   --|              | TEST  FNV_1 |
   --|              |_____________|
   --|              |             |
   --|
   --INITIALIZE
   Result := Fnv_1(S => Strg);
   Atio.Put("FNV_1         " & "16#" & Print_Integer(Result, 16, 16) & "#,");

   for W in 1 .. How_Many - 1 loop

      -- SEND THE RESULT BACK AS A STRING FOR THE NEXT_TEST
      Result := Fnv_1(S => Print_Integer(Result, 16, 16));
      Atio.Put("16#" & Print_Integer(Result    , 16, 16) & "#,");

   end loop;

   Atio.New_Line;
   --|
   --|              |____________________|
   --|              |                    |
   --|              | TEST SUPERFASTHASH |
   --|              |____________________|
   --|              |                    |
   --|
   --INITIALIZE
   Result := Superfasthash(Adata => Strg);
   Atio.Put("SUPERFASTHASH " & "16#" & Print_Integer(Result, 16, 16) & "#,");

   for W in 1 .. How_Many - 1 loop

      -- SEND THE RESULT BACK AS A STRING FOR THE NEXT_TEST
      Result := Superfasthash(Adata => Print_Integer(Result, 16, 16));
      Atio.Put("16#" & Print_Integer(Result                , 16, 16) & "#,");

   end loop;

   Atio.New_Line;
   --|
   --|              |______________|
   --|              |              |
   --|              | TEST XORSHIFT|
   --|              |______________|
   --|              |              |
   --|
   --INITIALIZE
   Result := Xorshift(Strg);
   Atio.Put("XORSHIFT      " & "16#" & Print_Integer(Result, 16, 16) & "#,");

   for W in 1 .. How_Many - 1 loop

      -- SEND THE RESULT BACK AS A STRING FOR THE NEXT_TEST
      Result := Xorshift(Print_Integer(Result, 16, 16));
      Atio.Put("16#" & Print_Integer(Result  , 16, 16) & "#,");

   end loop;

   Atio.New_Line;
   --|
   --|              |___________________|
   --|              |                   |
   --|              | TEST LONG_RANDOM  |
   --|              |___________________|
   --|              |                   |
   --|
   --INITIALIZE
   Result := Long_Random(Strg);
   Atio.Put("LONG_RANDOM   " & "16#" & Print_Integer(Result, 16, 16) & "#,");

   for W in 1 .. How_Many - 1 loop

      -- SEND THE RESULT BACK AS A STRING FOR THE NEXT_TEST
      Result := Long_Random(Print_Integer(Result, 16, 16));
      Atio.Put("16#" & Print_Integer(Result     , 16, 16) & "#,");

   end loop;

   Atio.New_Line;
   --|
   --|              |____________|
   --|              |            |
   --|              | TEST NHASH |
   --|              |____________|
   --|              |            |
   --|
   --INITIALIZE
   Result := Nhash(Strg);
   Atio.Put("NHASH         " & "16#" & Print_Integer(Result, 16, 16) & "#,");

   for W in 1 .. How_Many - 1 loop

      -- SEND THE RESULT BACK AS A STRING FOR THE NEXT_TEST
      Result := Nhash(Print_Integer(Result , 16, 16));
      Atio.Put("16#" & Print_Integer(Result, 16, 16) & "#,");

   end loop;

   Atio.New_Line;
   --|
   --|              |_____________|
   --|              |             |
   --|              | TEST SIPHASH|
   --|              |_____________|
   --|              |             |
   --|
   --INITIALIZE
   Result := Siphash(Strg);
   Atio.Put("SIPHASH       " & "16#" & Print_Integer(Result, 16, 16) & "#,");

   for W in 1 .. How_Many - 1 loop

      -- SEND THE RESULT BACK AS A STRING FOR THE NEXT_TEST
      Result := Siphash(Print_Integer(Result, 16, 16));
      Atio.Put("16#" & Print_Integer(Result , 16, 16) & "#,");

   end loop;

   Atio.New_Line;
   --|
   --|              |___________________|
   --|              |                   |
   --|              | TEST BALLOON_HASH |
   --|              |___________________|
   --|              |                   |
   --|
   --INITIALIZE
   Result := 2 ** 61 - 1;

   --INITIALIZE
   Result := Balloon_Hash(Strg & Print_Integer(Result, 16, 16),"SALT.SALT.. SALT");

   Atio.Put("BALLOON_HASH  " & "16#" & Print_Integer(Result, 16, 16) & "#,");

   for W in 1 .. How_Many - 1 loop

      -- SEND THE RESULT BACK AS A STRING FOR THE NEXT_TEST
      Result := Balloon_Hash(Print_Integer(Result, 16, 16),"SALT.SALT.. SALT");
      Atio.Put("16#" & Print_Integer(Result      , 16, 16) & "#,");

   end loop;

   Atio.New_Line;
   --|
   --|              |______________________|
   --|              |                      |
   --|              | TEST RIPEMD_320_HASH |
   --|              |______________________|
   --|              |                      |
   --|
   Test_Ripemd_320_Hash :
   declare
   H0 : U32 := 0;
   H1 : U32 := 0;
   H2 : U32 := 0;
   H3 : U32 := 0;
   H4 : U32 := 0;
   H5 : U32 := 0;
   H6 : U32 := 0;
   H7 : U32 := 0;
   H8 : U32 := 0;
   H9 : U32 := 0;
   begin
      Ripemd_320_Hash(Message => Strg,
                      H0      => H0  ,
                      H1      => H1  ,
                      H2      => H2  ,
                      H3      => H3  ,
                      H4      => H4  ,
                      H5      => H5  ,
                      H6      => H6  ,
                      H7      => H7  ,
                      H8      => H8  ,
                      H9      => H9);

      Atio.New_Line;
      Atio.Put("RIPEMD_320_HASH: "
               & Print_Integer(U64(H0), 16, 08)
               & Print_Integer(U64(H1), 16, 08)
               & "     "
               & Print_Integer(U64(H2), 16, 08)
               & Print_Integer(U64(H6), 16, 08)
               & "     "
               & Print_Integer(U64(H4), 16, 08)
               & Print_Integer(U64(H5), 16, 08)
               & "     "
               & Print_Integer(U64(H6), 16, 08)
               & Print_Integer(U64(H7), 16, 08)
               & "     "
               & Print_Integer(U64(H8), 16, 08)
               & Print_Integer(U64(H9), 16, 08));

      Atio.New_Line;
   end Test_Ripemd_320_Hash;

end Gaudry_Algorithms_Crypto_Hashings;
